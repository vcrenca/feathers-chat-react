import { configureStore, combineReducers } from '@reduxjs/toolkit';
import authReducer from '../features/auth/authSlice';
import chatReducer from '../features/chat/chatSlice'; 

export default configureStore({
  reducer: combineReducers({
    auth: authReducer,
    chat: chatReducer
  })
});
