import React from 'react'

const NoMatch = () => {
    return(
        <div>
            <h2>404 Not Found</h2>
            <p>Unfortunately the page that you requested does not exist...</p>
        </div>
    );
};

export default NoMatch;